import { HttpStatus, Injectable, Logger, OnModuleInit } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { PrismaClient } from '@prisma/client';
import { PaginationDto } from 'src/common';
import { RpcException } from '@nestjs/microservices';

@Injectable()
export class ProductsService extends PrismaClient implements OnModuleInit {
  private readonly logger = new Logger('ProductsService');

  onModuleInit() {
    this.$connect()
    this.logger.log('Database connection established')
  }

  create(createProductDto: CreateProductDto) {
    return this.product.create({
      data: createProductDto
    })
  }

  async findAll(paginationDto: PaginationDto) {
    const { page, limit } = paginationDto

    const totalPages = await this.product.count()

    const lastPage = Math.ceil(totalPages / limit)
    
    const data = await this.product.findMany({
      skip: (page - 1) * limit,
      take: limit,
      where: {
        deleteAt: null
      }
    })

    const metadata = {
      page,
      limit,
      pages: totalPages,
      lastPage
    }

    return { data, metadata }
  }

  async findOne(id: number) {
    const product = await this.product.findFirst({ 
      where: { id, deleteAt: null } 
    })

    if(!product) {
      throw new RpcException({
        message: `Product with id ${id} not found`,
        status: HttpStatus.BAD_REQUEST
      })
    }

    return product
  }

  async update(id: number, updateProductDto: UpdateProductDto) {
    await this.findOne(id)

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const { id: __, ...data } = updateProductDto
    
    const newProduct = await this.product.update({
      where: { id },
      data
    })

    return newProduct;
  }

  async remove(id: number) {
    await this.findOne(id)

    const removedProduct = await this.product.update({ 
      where: { id },
      data: { deleteAt: new Date() } 
    })

    return removedProduct;
  }
}
