import 'dotenv/config'
import * as joi from 'joi'

interface EnvVariables {
    PORT: number;
    DATABASE_URL: string
}

const envsSchema = joi.object({
    PORT: joi.number().default(3001),
    DATABASE_URL: joi.string().required(),
})
.unknown(true);

const { error, value } = envsSchema.validate(process.env);

if(error) {
    throw new Error(`Config validation error: ${error.message}`);
}

const envVars: EnvVariables = value;

export const envs = {
    port: envVars.PORT,
    databaseUrl: envVars.DATABASE_URL,
}
